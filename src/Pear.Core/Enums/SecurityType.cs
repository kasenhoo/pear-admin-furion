﻿using System.ComponentModel;

namespace Pear.Core
{
    /// <summary>
    /// 权限类型
    /// </summary>
    public enum SecurityType
    {
        /// <summary>
        /// 目录
        /// </summary>
        [Description("目录")]
        Directory = 1,
        /// <summary>
        /// 页面
        /// </summary>
        [Description("页面")]
        Menu = 2,
        /// <summary>
        /// 按钮
        /// </summary>
        [Description("按钮")]
        Button = 3
    }
}