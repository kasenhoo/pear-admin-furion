﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using Pear.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pear.Core.SeedData
{
    /// <summary>
    /// 用户表种子数据 <see cref="Security"/>
    /// </summary>
    public class SecuritySeedData : IEntitySeedData<Security>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<Security> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new Security
                {
                    Id = 2,
                    Name = "系统管理",
                    Sequence = 3,
                    Type = SecurityType.Directory,
                    Icon = "layui-icon-set-fill",
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2010,
                    ParentId = 2,
                    Name = "角色管理",
                    Url = "/admin/role/index",
                    Authorize = "admin.system.role:view",
                    Sequence = 1,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2011,
                    ParentId = 2010,
                    Name = "编辑",
                    Authorize = "admin.system.role:edit",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2012,
                    ParentId = 2010,
                    Name = "新增",
                    Authorize = "admin.system.role:add",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2013,
                    ParentId = 2010,
                    Name = "删除",
                    Authorize = "admin.system.role:delete",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2014,
                    ParentId = 2010,
                    Name = "分配权限",
                    Authorize = "admin.system.role:give",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },

                new Security
                {
                    Id = 2020,
                    ParentId = 2,
                    Name = "菜单管理",
                    Url = "/admin/security/index",
                    Authorize = "admin.system.security:view",
                    Sequence = 2,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2021,
                    ParentId = 2020,
                    Name = "编辑",
                    Authorize = "admin.system.security:edit",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2022,
                    ParentId = 2020,
                    Name = "新增",
                    Authorize = "admin.system.security:add",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2023,
                    ParentId = 2020,
                    Name = "删除",
                    Authorize = "admin.system.security:delete",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },


                new Security
                {
                    Id = 2030,
                    ParentId = 2,
                    Name = "数据字典",
                    Url = "/admin/dict/index",
                    Authorize = "admin.system.dict:view",
                    Sequence = 3,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2031,
                    ParentId = 2030,
                    Name = "编辑",
                    Authorize = "admin.system.dict:edit",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2032,
                    ParentId = 2030,
                    Name = "新增",
                    Authorize = "admin.system.dict:add",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 2033,
                    ParentId = 2030,
                    Name = "删除",
                    Authorize = "admin.system.dict:delete",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },



                new Security
                {
                    Id = 2040,
                    ParentId = 2,
                    Name = "系统日志",
                    Url = "/admin/log/index",
                    Authorize = "admin.system.log:view",
                    Sequence = 4,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 3,
                    Name = "系统工具",
                    Sequence = 4,
                    Type = SecurityType.Directory,
                    Icon = "layui-icon-app",
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 3010,
                    ParentId = 3,
                    Name = "开发文档",
                    Url = "/api/index.html",
                    Authorize = "admin.tool.apidoc:view",
                    Sequence = 1,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 3020,
                    ParentId = 3,
                    Name = "表单设计",
                    Url = "/admin/tool/formdev",
                    Authorize = "admin.tool.formdev:view",
                    Sequence = 2,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4,
                    Name = "单位组织",
                    Sequence = 2,
                    Type = SecurityType.Directory,
                    Icon = "layui-icon-user",
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4010,
                    ParentId = 4,
                    Name = "用户管理",
                    Url = "/admin/user/index",
                    Authorize = "admin.usercenter.user:view",
                    Sequence = 1,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4011,
                    ParentId = 4010,
                    Name = "编辑用户",
                    Authorize = "admin.usercenter.user:edit",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4012,
                    ParentId = 4010,
                    Name = "新增用户",
                    Authorize = "admin.usercenter.user:add",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4013,
                    ParentId = 4010,
                    Name = "删除用户",
                    Authorize = "admin.usercenter.user:delete",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4014,
                    ParentId = 4010,
                    Name = "用户详情",
                    Authorize = "admin.usercenter.user:info",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4015,
                    ParentId = 4010,
                    Name = "用户角色查看",
                    Authorize = "admin.usercenter.user:roles",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4016,
                    ParentId = 4010,
                    Name = "用户角色分配",
                    Authorize = "admin.usercenter.user:giveroles",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4017,
                    ParentId = 4010,
                    Name = "用户权限菜单",
                    Authorize = "admin.usercenter.user:securities",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },


                new Security
                {
                    Id = 4020,
                    ParentId = 4,
                    Name = "部门管理",
                    Url = "/admin/department/index",
                    Authorize = "admin.usercenter.dept:view",
                    Sequence = 2,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4021,
                    ParentId = 4020,
                    Name = "编辑",
                    Authorize = "admin.usercenter.dept:edit",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4022,
                    ParentId = 4020,
                    Name = "新增",
                    Authorize = "admin.usercenter.dept:add",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id = 4023,
                    ParentId = 4020,
                    Name = "删除",
                    Authorize = "admin.usercenter.dept:delete",
                    Sequence = 1,
                    Type = SecurityType.Button,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
                new Security
                {
                    Id=5,
                    Name="工作空间",
                    Sequence= 1,
                    Type = SecurityType.Directory,
                    Icon="layui-icon-console" ,
                    CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted=false,
                    Enabled=true
                },
                new Security
                {
                    Id=5010,
                    ParentId=5,
                    Name="控制台",
                    Url="/admin/home/console",
                    Authorize="admin.home.console:view",
                    Sequence= 1,
                    Type = SecurityType.Menu ,
                    CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted=false,
                    Enabled=true
                },
                new Security
                {
                    Id = 5020,
                    ParentId = 5,
                    Name = "服务器信息",
                    Url = "/admin/home/server",
                    Authorize = "admin.home.server:view",
                    Sequence = 2,
                    Type = SecurityType.Menu,
                    CreatedTime = DateTimeOffset.Parse("2020-12-17 10:00:00"),
                    IsDeleted = false,
                    Enabled = true
                },
            };
        }



    }
}