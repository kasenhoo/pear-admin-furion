﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Pear.Core.SeedData
{
    /// <summary>
    /// 部门表种子数据 <see cref="Role"/>
    /// </summary>
    public class DepartmentSeedData : IEntitySeedData<Department>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<Department> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new Department
                {
                    Id=1,Name="武汉总公司",Remark="公司目录", CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=101,Name="行政管理部",Remark="部门目录", ParentId=1,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=102,Name="财务部",Remark="部门目录",ParentId=1,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=103,Name="技术部",Remark="部门目录",ParentId=1,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=104,Name="市场部",Remark="部门目录",ParentId=1,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=105,Name="后勤部",Remark="部门目录",ParentId=1,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=2,Name="北京分公司",Remark="公司目录",CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },  
                new Department
                {
                    Id=201,Name="市场部",Remark="部门目录",ParentId=2,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=202,Name="后勤部",Remark="部门目录",ParentId=2,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=3,Name="上海分公司",Remark="公司目录",CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                  new Department
                {
                    Id=301,Name="市场部",Remark="部门目录",ParentId=3,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
                new Department
                {
                    Id=302,Name="后勤部",Remark="部门目录",ParentId=3,CreatedTime=DateTimeOffset.Parse("2020-12-17 10:00:00"),IsDeleted=false,Enabled=true
                },
            };
        }
    }
}